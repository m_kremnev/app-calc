export interface FieldProps {
	field: boolean[][];
	onClick: (x: number, y: number) => void;
}
